��    ;      �      �      �     �  %   �                  
   .     9     B     P  	   f  	   p     z     �     �  
   �     �  
   �     �  Y   �     &     ,  "   3  D   V     �     �  �   �  
   4     ?     U     d     l     y  	   �     �     �     �     �     �     �     �               +     9     O  
   j     u  	   �     �      �     �     �     �  
   �     �     �            �         	  ,   	     J	     a	     x	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	     
     
     ,
  	   >
  Y   H
     �
     �
  1   �
  F   �
     *     7  �   <  
   �     �                     2     A     N  $   e     �  #   �     �     �     �     �     
          )     =     Z     g     v     �  5   �     �     �     �     �  	     	             8   (No calendar) A Calendar is a collection of events. Add New Calendar Add New Event All Calendars All Events Calendar Calendar Type Calendar subscription Calendar: Calendars Create new Event Daily Date and Time Don't sync Edit Calendar Edit Event End Date Enter the URL of the calendar. The Application will only accept data in vCalendar format. Event Events Events are gathered in a calendar. Events in this Calendar will appear in all calendars in the Network. Every 30 Days HTML In a calendar subscription you specify an URL from where to load the events. In a local calendar you create events right on your blog Joern Lund Last Sync: %1$s, %2$s Local calendar Network Never synced New Calendar New Event No calendars found No calendars found in trash No event found No event found in trash Once Weekly Once a Year Remote Settings Remote URL: Search Calendars Search Events See this calandar in  Simple Wordpress Calendar. Start Date Sync Calendar: Sync now! Syncing This is a Network wide calendar. This is a full day event Time View Calendar View Event calendarAdd new eventAdd new parent Calendar to Project-Id-Version: WP-Calendula v0.9.0b
PO-Revision-Date: 2013-10-02 13:49:36+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: CSL v1.x (Kein Kalender) Ein Kalender ist einen Sammlung von Terminen Neuen Kalender hinzuf. Neuen Termin erstellen Alle Kalender Alle Termine Kalender Kalender-Typ Kalender Abonnement Kalender: Kalender Neuen Termin erstellen Täglich Datum und Uhrzeit Nicht Synchronisieren Kalender bearbeiten Termin bearbeiten End Datum Tragen Sie die URL des Kalenders ein. Es werden nur Daten im iCalendar-Format akzeptiert. Termin Termine Termine werden in einem Kalender zusammengefasst. Die Termine in diesem Kalender erschienen auf allen Blogs im Netzwerk. Alle 30 Tage HTML In einem Kalender Abonnement tragen Sie eine URL ein, von der die Termine geladen werden. In einem lokalen Kalender erstellen Sie Termine direkt in Ihrem Blog. Jörn Lund Letzter Sync: %1$s, %2$s Lokaler Kalender Netzwerk Noch nie synchronisiert Neuer Kalender Neuer Termin Kein Kalender gefunden Kein Kalender im Papierkorb gefunden Kein Termin gefunden Keine Termin im Papierkorb gefunden Einmal wöchentlich Einmal Jährlich Abo-Einstellungen Abonnement-URL: Kalender suchen Termine suchen Kalender im Format: Einfacher WordPress Kalender Beginn Datum Kalender Sync. Jetzt Syncen! Synchronisierung Diesen Kalender im gesamten Netzwerk veröffentlichen Ganztägiger Termin Uhrzeit Kalender anschauen Termin anschauen Erstellen Erstellen Übergeordneter Kalender bis 